﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

	private AudioManager m_Instance;
	public AudioSource m_AudioSource;
	public AudioClip m_AudioClip;
	public bool m_SoundIsActive;

	void Awake(){
		DontDestroyOnLoad(this.transform);
		GetInstance();
		m_SoundIsActive = true;
	}

	public AudioManager GetInstance(){
		if(m_Instance == null){
			m_Instance = this;
			m_AudioSource = this.GetComponent<AudioSource>();
			if(m_AudioSource == null){
				m_AudioSource = gameObject.AddComponent<AudioSource>();
				m_AudioSource.clip = m_AudioClip;
				m_AudioSource.Play();
			}
		}

		return m_Instance;
	}

	public void TogglePlay(){
		if(m_AudioSource.isPlaying){
			m_AudioSource.Stop();
			m_SoundIsActive = false;
		} else {
			m_AudioSource.Play();
			m_SoundIsActive = true;
		}
	}

	public void SetPlay(bool _Playing){
		if(_Playing){
			m_AudioSource.Play();
		} else {
			m_AudioSource.Pause();
		}
	}


	public void SetAudioSource(AudioClip _Clip){
		m_AudioClip = _Clip;
		m_AudioSource.clip = _Clip;
	}

	public AudioClip GetAudioSource(){
		return m_AudioSource.clip;
	}
}
