﻿using UnityEngine;
using System.Collections;

public class EnemyDropper : MonoBehaviour {

	public float m_TimeBetweenDrops;
	public float m_TimeTillNextDrop;

	public int m_MaxDrops;
	public int m_CurrentDrops;

	public GameObject m_Chicken;
	public GameObject m_Plop;
	private Camera m_Camera;

	void Awake(){
		m_TimeTillNextDrop = m_TimeBetweenDrops;
		m_CurrentDrops = 0;
		m_Camera = Camera.main;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		m_TimeTillNextDrop -= Time.deltaTime;
		if(m_TimeTillNextDrop < 0.0f){
			m_TimeTillNextDrop = m_TimeBetweenDrops;
			if(m_CurrentDrops < m_MaxDrops){
				float horizontalSize = m_Camera.orthographicSize * Screen.width / Screen.height;
				float leftBorder = m_Camera.transform.position.x - horizontalSize;
				float rightBorder = m_Camera.transform.position.x + horizontalSize;
				float newX = Random.Range(leftBorder, rightBorder);
				Instantiate(m_Plop, new Vector3(newX, -5.1f, 0.0f), Quaternion.identity);
				m_CurrentDrops ++;
			}
		}
	}
}
