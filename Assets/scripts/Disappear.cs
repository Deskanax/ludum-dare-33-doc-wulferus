﻿using UnityEngine;
using System.Collections;

public class Disappear : MonoBehaviour {

	public GameObject m_Plop;
	public float m_TTL;
	public float m_CurrentLifetime;
	private EnemyDropper m_EnemyDropper;

	void Awake(){
		m_CurrentLifetime  = m_TTL;
		m_EnemyDropper = GameObject.FindGameObjectWithTag("GameController").GetComponent<EnemyDropper>();
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		m_CurrentLifetime -= Time.deltaTime;
		if(m_CurrentLifetime < 0.0f){
			Kill();
		}
	}

	public void Kill(){
		Instantiate(m_Plop, this.transform.position, Quaternion.identity);
		m_EnemyDropper.m_CurrentDrops --;
		Destroy(this.gameObject);
	}
}
