﻿using UnityEngine;
using System.Collections;

public class AudioControl : MonoBehaviour {

	private Animator m_Animator;
	public AudioSource m_AudioSource;

	public AudioClip m_WalkingSound;
	public AudioClip m_AttackSound;

	void Awake(){
		m_Animator = GetComponent<Animator>();
		m_AudioSource = GetComponent<AudioSource>();
	}

	// Update is called once per frame
	void Update () {
		if (m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Walk")) { 
			m_AudioSource.clip = m_WalkingSound;
			if(!m_AudioSource.isPlaying){
				m_AudioSource.loop = true;
				m_AudioSource.Play ();
			}
		}
		if (m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Hit")) { 
			m_AudioSource.clip = m_AttackSound;
			if(!m_AudioSource.isPlaying){
				m_AudioSource.loop = false;
				m_AudioSource.Play ();
			}
		}

		if(m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Idle")) {
			if(m_AudioSource.isPlaying){
				m_AudioSource.Stop ();
			}
		}
	}
}
