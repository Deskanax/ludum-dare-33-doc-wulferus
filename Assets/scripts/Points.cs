﻿using UnityEngine;
using System.Collections;

public class Points : MonoBehaviour {

	private int m_Points;


	void Awake(){
		m_Points = 0;
	}

	public void IncPoints(int _IncVal){
		m_Points += _IncVal;
	}

	public int GetPoints(){
		return m_Points;
	}
}
