﻿using UnityEngine;
using System.Collections;

public class Random_View : MonoBehaviour {

	private float m_Cooldown;
	private float m_CurrentCooldown;

	void Awake(){
		m_Cooldown = 4.0f;
		m_CurrentCooldown = m_Cooldown;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		m_CurrentCooldown -= Time.deltaTime;
		if(m_CurrentCooldown < 0.0f){
			m_CurrentCooldown = m_Cooldown;
			bool left = Random.Range(0, 1) == 0 ? true : false;
			Vector3 scale = this.transform.localScale;
			scale.x = Mathf.Abs(scale.x);

			if(left){
				scale.x = (-1.0f) *  scale.x;
			}
			this.transform.localScale = scale;
		}
	}
}
