using UnityEngine;
using System.Collections;

public class GUIUtility : MonoBehaviour
{

	public static float ShrinkFloat(float _Float){
		return Mathf.Floor(_Float * 100.0f) / 100.0f;
	}
}

