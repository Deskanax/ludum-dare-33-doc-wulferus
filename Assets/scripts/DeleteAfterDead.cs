﻿using UnityEngine;
using System.Collections;

public class DeleteAfterDead : MonoBehaviour {

	private Animator m_Animator;
	private AudioSource m_AudioSource;
	
	// Use this for initialization
	void Awake () {
		m_Animator = GetComponent<Animator>();
		m_AudioSource = GetComponent<AudioSource>();
		m_AudioSource.pitch =  Random.Range(0.8f, 1.2f);
		Vector3 localRot = this.transform.localRotation.eulerAngles;
		localRot.z += Random.Range(0.0f, 90.0f);
		this.transform.localRotation = Quaternion.Euler(localRot);
	}
	
	// Update is called once per frame
	void Update () {
		if(m_Animator.GetCurrentAnimatorStateInfo(0).IsName("End")){
			GetComponent<SpriteRenderer>().sprite = null;
		if (!m_AudioSource.isPlaying){
			Destroy(this.gameObject);
		}
		}
	}
}
