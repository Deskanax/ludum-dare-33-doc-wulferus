﻿using UnityEngine;
using System.Collections;

public class HitScript : MonoBehaviour {
	
	private Animator m_CurrentAnimator; 
	private GameManager m_GameManager;

	void Awake(){
		m_CurrentAnimator = GetComponent<Animator>();
		m_GameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D _Other){
		if(m_CurrentAnimator.GetCurrentAnimatorStateInfo(0).IsName("Hit")){
			Debug.Log("Ducky hit");

			if(_Other.CompareTag("Enemy")){
				m_GameManager.IncPoints(_Other.GetComponent<Ducky>().m_PointsWhenDead);
				_Other.GetComponent<Disappear>().Kill();
			}
		}

	}

}
