﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
	private bool m_FacingRight;
	private Rigidbody2D m_RigidBody;
	private Sprite m_PlayerSprite;
	private Animator m_Animator; 
	public Player m_Player;

	void Awake(){
		m_RigidBody = GetComponent<Rigidbody2D>();
		m_Player = GetComponent<Player>();
		m_FacingRight = true;
		m_PlayerSprite = GetComponent<Sprite>();
		m_Animator = GetComponent<Animator>();
	}

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		Vector2 velocity = m_Player.m_SpeedAddVelocity;
		if(Input.GetKey(KeyCode.LeftArrow)){
			m_RigidBody.AddForce((-1.0f) * velocity);
		} else if(Input.GetKey(KeyCode.RightArrow)){
			m_RigidBody.AddForce(velocity);
		} else if(Input.GetKey(KeyCode.Space)){
			m_Animator.SetBool("hit", true);
		}
		velocity  = m_RigidBody.velocity;
		if(velocity.x > 0.0f){
			velocity.x = Mathf.Min(velocity.x, m_Player.m_MaxSpeed.x);
		} else if(velocity.x < 0.0f){
			velocity.x = Mathf.Max(velocity.x, (-1.0f) * m_Player.m_MaxSpeed.x);
		}
		m_RigidBody.velocity = velocity;
	
		if(velocity.x < 0.0f && m_FacingRight){
			m_FacingRight = false;
			Vector3 scale = transform.localScale;
			scale.x  = (-1.0f) * scale.x;
			transform.localScale = scale;
		} else if(velocity.x > 0.0f && !m_FacingRight){
			m_FacingRight = true;
			Vector3 scale = transform.localScale;
			scale.x  = (-1.0f) * scale.x;
			transform.localScale = scale;
		} else if(velocity.x < 0.25f && velocity.x > -0.25f){
			m_Animator.SetBool("walking", false);
		} 

		if(velocity.x >= 0.25f || velocity.x <= -0.25f){
			m_Animator.SetBool("walking", true);
		}

		if (m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Hit")) { 
			if(!GetComponent<AudioControl>().m_AudioSource.isPlaying){
				m_Animator.SetBool("hit", false);
			}
		} 
	}
}
