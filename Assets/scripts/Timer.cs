﻿using UnityEngine;
using System.Collections;

public class Timer : MonoBehaviour {

	public float m_MaxTime;
	public float m_CurrentTimeLeft;
	private GameManager m_GameManager;

	void Awake(){
		m_CurrentTimeLeft = m_MaxTime;
		m_GameManager = GetComponent<GameManager>();
	}
	
	// Update is called once per frame
	void Update () {
		m_CurrentTimeLeft -= Time.deltaTime;
		if(m_CurrentTimeLeft <= 0.0f){
			m_CurrentTimeLeft = 0.0f;
			m_GameManager.m_Done = true;
		}
	}
}
