﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	private Points m_Points;
	private Timer m_Timer;

	public Text m_GuiPoints;
	public Text m_GuiTimeLeft;
	public Transform m_Endscreen;
	public Text m_EndscreenPoints;

	/// <summary>
	/// Indicates if game is done. Is changed externally.
	/// </summary>
	public bool m_Done;

	void Awake(){
		m_Points = GetComponent<Points>();
		m_Timer = GetComponent<Timer>();
		m_Endscreen.GetComponent<Canvas>().enabled = false;
		m_Done = false;
	}

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

		m_GuiPoints.text = m_Points.GetPoints().ToString();
		m_GuiTimeLeft.text = System.Math.Round ((double) m_Timer.m_CurrentTimeLeft, 2).ToString() + " s";

		if(m_Done){
			// simple way to pause but have some effects that you won't want.
			Time.timeScale = 0.9f;
			// do some ui stuff in here for showing result.
			m_Endscreen.gameObject.GetComponent<Canvas>().enabled = true;
			GameObject player = GameObject.FindGameObjectWithTag("Player");
			if(player != null){
				Destroy(player);
				m_EndscreenPoints.text = m_Points.GetPoints().ToString();
			}
		}
	}

	public void IncPoints(int _PointAmount){
		m_Points.IncPoints(_PointAmount);
	}

}
