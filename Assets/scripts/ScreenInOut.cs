﻿using UnityEngine;
using System.Collections;

public class ScreenInOut : MonoBehaviour {

	private Camera m_Camera;


	void Awake(){
		m_Camera = Camera.main;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float horizontalSize = m_Camera.orthographicSize * Screen.width / Screen.height;
		float leftBorder = m_Camera.transform.position.x - horizontalSize;
		float rightBorder = m_Camera.transform.position.x + horizontalSize;

		Vector3 pos = this.transform.position;

		if(pos.x < leftBorder){
			pos.x = rightBorder;
		} else if(pos.x > rightBorder){
			pos.x = leftBorder;
		}
		this.transform.position = pos;
	}
}
