﻿using UnityEngine;
using System.Collections;

public class DeleteAfterAnim : MonoBehaviour {
	public GameObject m_Chicken;
	private Animator m_Animator;
	private AudioSource m_AudioSource;

	// Use this for initialization
	void Awake () {
		m_Animator = GetComponent<Animator>();
		m_AudioSource = GetComponent<AudioSource>();
		m_AudioSource.pitch =  Random.Range(0.8f, 1.2f);
	}
	
	// Update is called once per frame
	void Update () {
		if(m_Animator.GetCurrentAnimatorStateInfo(0).IsName("End") && !m_AudioSource.isPlaying){
			Instantiate(m_Chicken, this.transform.position, Quaternion.identity);
			Destroy(this.gameObject);
		}
	}
}
